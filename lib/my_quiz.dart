import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

class TileInfo {
  String title;
  Color color;

  TileInfo({required this.title, required this.color});
}

List<TileInfo> listTileInfo = [
  TileInfo(title: "Pink", color: Colors.pinkAccent),
  TileInfo(title: "Cyan", color: Colors.cyanAccent),
  TileInfo(title: "Orange", color: Colors.deepOrangeAccent),
  TileInfo(title: "Green", color: Colors.greenAccent),
];

int score = 0;

class MyQuiz extends StatefulWidget {
  const MyQuiz({Key? key}) : super(key: key);

  @override
  State<MyQuiz> createState() => _MyQuizState();
}

class _MyQuizState extends State<MyQuiz> {
  int currentPage = 0;
  List<bool> yesOrNoList = List.generate(10, (index) => Random().nextBool());
  List<bool> yesOrNoAnswerList = List.generate(10, (index) => false);

  Widget _customListTile({
    required String title,
    required Color color,
  }) =>
      ListTile(
        title: Text(title),
        tileColor: color,
        onTap: () {
          if (color == Colors.pinkAccent) {
            score = 1;
            print("$title has been chosen with score: $score");
          } else {
            score = 0;
          }
          setState(() {
            currentPage++;
          });
        },
      );

  @override
  Widget build(BuildContext context) {
    bool isOnlyYesChecked = true;

    return Scaffold(
      appBar: AppBar(
        title: const Text("My Quiz"),
      ),
      body: currentPage == 0
          ? Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage("images/drone.png"),
                  colorFilter: ColorFilter.mode(
                    Colors.white.withOpacity(0.3),
                    BlendMode.modulate,
                  ),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const SizedBox(height: 10),
                    const Center(
                      child: Text(
                        "Select the pink rectangle!",
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ),
                    const Spacer(flex: 2),
                    ...List.generate(
                      listTileInfo.length,
                      (index) => _customListTile(
                        title: listTileInfo[index].title,
                        color: listTileInfo[index].color,
                      ),
                    ),
                    const Spacer(flex: 1),
                  ],
                ),
              ),
            )
          : currentPage == 1
              ? Column(
                  children: [
                    const SizedBox(height: 20),
                    const Center(
                      child: Text(
                        "Check only the 'YES' tile!",
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ),
                    const SizedBox(height: 30),
                    ...List.generate(
                      yesOrNoList.length,
                      (index) {
                        bool isMatch = yesOrNoAnswerList[index] == yesOrNoList[index];
                        isOnlyYesChecked = const ListEquality().equals(
                          yesOrNoAnswerList,
                          yesOrNoList,
                        );

                        return CheckboxListTile(
                          title: Text(yesOrNoList[index] ? "YES" : "NO"),
                          value: yesOrNoAnswerList[index],
                          tileColor: isMatch ? Colors.greenAccent : Colors.redAccent,
                          onChanged: (value) {
                            if (value != null) {
                              setState(() {
                                yesOrNoAnswerList[index] = value;
                              });
                            }
                          },
                        );
                      },
                    ),
                    const Spacer(),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 32),
                      width: double.infinity,
                      child: TextButton(
                        onPressed: () {
                          setState(() {
                            isOnlyYesChecked ? score++ : null;
                            currentPage++;
                          });
                        },
                        child: const Text(
                          "Finish",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    const Spacer(),
                  ],
                )
              : Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 32),
                        child: Image.asset(
                          "images/deer.png",
                          width: 192,
                          fit: BoxFit.cover,
                        ),
                      ),
                      const SizedBox(height: 20),
                      Text(
                        "Your Score: $score",
                        style: const TextStyle(fontSize: 24),
                      ),
                      const SizedBox(height: 50),
                    ],
                  ),
                ),
    );
  }
}
